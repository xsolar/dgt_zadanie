CREATE TABLE fees_for_transactions (   id                INTEGER PRIMARY KEY,
                                       transaction_type   VARCHAR(64) NOT NULL,
                                       fees              VARCHAR(64) NOT NULL);