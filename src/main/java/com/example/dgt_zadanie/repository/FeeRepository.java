package com.example.dgt_zadanie.repository;

import com.example.dgt_zadanie.data.FeesForTransactions;
import org.springframework.data.repository.CrudRepository;

public interface FeeRepository extends CrudRepository<FeesForTransactions, Long> {
}
