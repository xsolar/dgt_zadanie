package com.example.dgt_zadanie.utils;

import javax.persistence.AttributeConverter;
import java.util.Collections;
import java.util.List;
import java.util.Set;

public class StringSetConverter implements AttributeConverter<List<String>, String> {
    private static final String SPLIT_CHAR = ";";

    @Override
    public String convertToDatabaseColumn(List<String> stringList) {
        return stringList != null ? String.join(SPLIT_CHAR, stringList) : "";
    }

    @Override
    public List<String> convertToEntityAttribute(String string) {
        return string != null ? List.of(string.split(SPLIT_CHAR)) : Collections.emptyList();
    }
}