package com.example.dgt_zadanie.data;

import com.example.dgt_zadanie.utils.StringSetConverter;

import javax.persistence.*;
import java.util.List;

@Entity
public class FeesForTransactions {

    @Id
    @GeneratedValue
    private Long id;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    @Column(name = "transaction_type", nullable = false, length = 30)
    private String transactionType;

    @Convert(converter = StringSetConverter.class)
    private List<String> fees;

    public String getTransactionType() {
        return transactionType;
    }

    public void setTransactionType(String transactionType) {
        this.transactionType = transactionType;
    }

    public List<String> getFees() {
        return fees;
    }

    public void setFees(List<String> fees) {
        this.fees = fees;
    }
}
