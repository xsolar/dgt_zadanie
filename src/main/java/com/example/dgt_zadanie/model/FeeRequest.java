package com.example.dgt_zadanie.model;

import java.util.List;

public record FeeRequest(List<Transaction> transactions) {
}
