package com.example.dgt_zadanie.model;

public record Transaction(String transactionType, String transactionId, String transactionDetail) {
}
