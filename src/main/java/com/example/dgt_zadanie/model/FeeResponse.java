package com.example.dgt_zadanie.model;

public record FeeResponse(Long calculatedSum) {
}
