package com.example.dgt_zadanie.service;

import com.example.dgt_zadanie.model.FeeRequest;
import com.example.dgt_zadanie.model.FeeResponse;

public interface FeeService {

    FeeResponse getFeesForTransactions(FeeRequest request);
}
