package com.example.dgt_zadanie.web;

import com.example.dgt_zadanie.model.FeeRequest;
import com.example.dgt_zadanie.model.FeeResponse;
import com.example.dgt_zadanie.service.FeeService;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FeeController {

    private final FeeService feeService;

    public FeeController(FeeService feeService) {
        this.feeService = feeService;
    }

    @PostMapping(value = "/fees", consumes = MediaType.APPLICATION_JSON_VALUE)
    public FeeResponse calculateFees(@RequestBody FeeRequest requestBody) {
        return feeService.getFeesForTransactions(requestBody);
    }
}
