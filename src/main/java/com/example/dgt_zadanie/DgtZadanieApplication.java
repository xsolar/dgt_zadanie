package com.example.dgt_zadanie;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DgtZadanieApplication {

    public static void main(String[] args) {
        SpringApplication.run(DgtZadanieApplication.class, args);
    }

}
