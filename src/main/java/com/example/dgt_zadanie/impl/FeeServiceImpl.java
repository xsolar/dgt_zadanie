package com.example.dgt_zadanie.impl;

import com.example.dgt_zadanie.data.FeesForTransactions;
import com.example.dgt_zadanie.model.FeeRequest;
import com.example.dgt_zadanie.model.FeeResponse;
import com.example.dgt_zadanie.model.Transaction;
import com.example.dgt_zadanie.repository.FeeRepository;
import com.example.dgt_zadanie.service.FeeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FeeServiceImpl implements FeeService {

    private final FeeRepository repository;


    public FeeServiceImpl(FeeRepository javaScriptFrameworkRepository) {
        repository = javaScriptFrameworkRepository;
    }

    @Override
    public FeeResponse getFeesForTransactions(FeeRequest request) {
        Iterable<FeesForTransactions> fees = repository.findAll();
        List<String> transactionTypes = request.transactions().stream()
                .map(Transaction::transactionType)
                .toList();
        long result = 0L;
        for (String transactionType : transactionTypes){
            for (FeesForTransactions fee : fees) {
                if (fee.getTransactionType().equals(transactionType)) {
                    result = fee.getFees().stream()
                            .mapToLong(Long::parseLong)
                            .sum();
                }
            }
        }
        return new FeeResponse(result);
    }


}
